## Комментарии
Данные по остатку обновляются при помощи команды `app/Console/Commands/GetBalance.php` с использованием планировщика задач Laravel

#### Файлы, которые необходимо было реализовать:
 * Файл маршрутов - `routes/web.php`
 * Файл миграции - `database/migrations/2019_01_16_073731_create_balances_table.php`
 * Файл модели - `app/Balance.php`
 * Файл класса - `app/Services/RuTaxiCrawler.php`
 * Файл контроллера - `app/Http/Controllers/TestPageController.php`
 * Файл шаблона - `resources/views/balance.blade.php`