<?php

namespace App\Http\Controllers;

use App\Balance;

class TestPageController extends Controller
{
    public function index()
    {
        $balance = null;
        $balanceEntity = Balance::orderBy('created_at', 'desc')->first();
        if ($balanceEntity) {
            $balance = $balanceEntity->balance;
        }
        
        return view('balance', [
            'balance' => $balance
        ]);
    }
}
