<?php
namespace App\Services;

use Goutte\Client;

class RuTaxiCrawler
{
    protected $link = 'https://spb.rutaxi.ru/car_owners.html';
    
    protected $login = '988799';
    
    protected $password = '136';

    /**
     * 
     * @return float Account balance
     */
    public function getAccountBalance()
    {
        $crawler = $this->login();
        $selector = '#col1 .wrapper .row .col-md-9 .dl-horizontal dd';
        $balanceText = $crawler->filter($selector)->text();
        
        $balanceTextParts = explode(' ', $balanceText);
        $balance = null;
        foreach ($balanceTextParts as $part) {
            if (!is_numeric($part)) {
                continue;
            }
            
            if ($balance === null) {
                $balance = $part;
            } else {
                $balance += $part/100;
            }
        }
        
        return floatval($balance);
    }
    
    /**
     * 
     * @return Symfony\Component\DomCrawler\Crawler
     */
    protected function login() {
        // makes a real request to an external site
        $client = new Client();
        $crawler = $client->request('GET', $this->link);

        // select the form and fill in some values
        $form = $crawler->selectButton('Войти')->form();
        $form['login'] = $this->login;
        $form['password'] = $this->password;

        // submits the given form
        return $client->submit($form);
    }
}
