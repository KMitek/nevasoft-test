<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\RuTaxiCrawler;
use App\Balance;

class GetBalance extends Command
{
    protected $crawler;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'balance:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get and save account balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RuTaxiCrawler $crawler)
    {
        parent::__construct();
        
        $this->crawler = $crawler;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $balance = $this->crawler->getAccountBalance();
        
        Balance::create(['balance' => $balance]);
    }
}
